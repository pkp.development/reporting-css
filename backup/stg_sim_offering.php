<?php

// $koneksi  = mysqli_connect("192.168.0.183", "simgroup", "Sh@d0wW@v3", "core");
$koneksi  =  mysqli_connect("149.129.198.33", "dbsimbpr", "$1mgr0up@2021", "core");
$koneksi2 = mysqli_connect("localhost", "pkp", "Pr@w4th1y4", "css");

// hapus dulu tabel bersangkutan sebelum di insert ulang 
$sqlDelete   = "DELETE FROM stg_sim_offering";
mysqli_query($koneksi2,$sqlDelete);
// echo "sukses Dihapus";
// die();
// ---
$data = mysqli_query($koneksi,"SELECT aa.id_so_parent as id_so_induk,aa.nomor_so_parent as nomor_so_induk,a.kode as jo_number,
g.`name` as nama_candidate,
e.id as id_client,e.nomor_akun as kode_client,e.nama as nama_client,
d.id_address as id_cabang_client,d.kode as kode_cabang_client,d.nama_bagian as cabang_client,
c.id as id_jabatan,
c.`subject` as nama_jabatan,
CASE 
          WHEN af.cabang IS NOT NULL THEN af.cabang
          ELSE 'JAKARTA(HO)'
        END AS coverage_sim,
CASE ((a.age_from OR a.age_to) IS NOT NULL) OR ((a.age_from OR a.age_to) = 0)
          WHEN (a.age_from OR a.age_to) IS NOT NULL THEN CONCAT(a.age_from,'-',a.age_to) 
          WHEN (a.age_from OR a.age_to) = 0 THEN CONCAT(a.age_from,'-',a.age_to)
          ELSE '18-27'
        END AS usia, 
CASE a.gender
          WHEN 'semua' THEN 'Pria / Wanita' 
          WHEN 'pria' THEN 'Pria'
          WHEN 'wanita' THEN 'Wanita'
          ELSE 'Pria / Wanita'
        END AS jenis_kelamin,
DATE_FORMAT(f.approve_date,'%d %M %Y') as tanggal_kirim_candidate,
DATE_FORMAT(ab.create_date,'%d %M %Y') as tanggal_interview,
DATE_FORMAT(ac.create_date,'%d %M %Y') as tanggal_psikotest,
CASE 
          WHEN f.ket = 0 THEN 'Lulus' 
          WHEN f.ket <> 0 THEN 'Tidak Lulus'
          ELSE ''
        END AS hasil_seleksi_client,

CASE 
          WHEN f.ket = 0 THEN '' 
          WHEN f.ket <> 0 THEN ae.namaData
          ELSE ''
        END AS remark


FROM so_job_posisi a LEFT JOIN so_data b ON a.id_so=b.id_so
LEFT JOIN rec_job_posisi c ON a.job_posisi=c.id
LEFT JOIN client_address d ON a.penempatan=d.id_address
LEFT JOIN client_data e ON b.client=e.id
/*LEFT JOIN rec_selection_candidate f on a.id_job=f.id_posisi*/
LEFT JOIN rec_selection_candidate f on (a.id_job=f.id_posisi AND a.id_so=f.id_so)
LEFT JOIN rec_applicant g ON f.parent_id=g.id

LEFT JOIN master_nomor_so aa ON b.id_so=aa.id_so

LEFT JOIN (SELECT DISTINCT 
            parent_id,create_date,
            MAX(id) AS id
        FROM 
            rec_selection_appl
				WHERE phase_id='RTS004'
        GROUP BY 
            parent_id ) ab ON g.id=ab.parent_id
LEFT JOIN (SELECT DISTINCT 
            parent_id,create_date,
            MAX(id) AS id
        FROM 
            rec_selection_appl
				WHERE phase_id='RTS002'
        GROUP BY 
            parent_id ) ac ON g.id=ac.parent_id
LEFT JOIN (SELECT DISTINCT 
            parent_id,sel_status,
            MAX(id) AS id
        FROM 
            rec_selection_appl
				WHERE phase_id='RTS006'
        GROUP BY 
            parent_id ) ad ON g.id=ad.parent_id
LEFT JOIN mst_data ae ON f.ket=ae.kodeData
LEFT JOIN (SELECT DISTINCT 
            lokasi_kerja,id_perusahaan,cabang,
            MAX(id) AS id
        FROM 
            bisnis_network
        GROUP BY 
            lokasi_kerja ) af ON d.lokasi_kerja=af.lokasi_kerja
WHERE (aa.nomor_so_parent IS NOT NULL) AND (a.kode IS NOT NULL) AND (e.nama IS NOT NULL) AND (d.nama_bagian IS NOT NULL)
order by b.nomor_so desc");

// echo "<pre>";
// $row=mysqli_fetch_array($data);
// $jum  = count($row);
// echo $jum . "  : " ;
// die();
// print_r($row=mysqli_fetch_array($data));
// echo "ggg";
// die();
$x = 0 ;
while ($row=mysqli_fetch_array($data))
{
  $x++ ;
    $id_so_induk              = $row['id_so_induk']; 
    $nomor_so_induk           = $row['nomor_so_induk']; 
    $jo_number                = $row['jo_number'];
    $nama_candidate           = $row['nama_candidate'];
    $id_client                = $row['id_client'];
    $kode_client              = $row['kode_client']; 
    $nama_client              = $row['nama_client'];
    $id_cabang_client         = $row['id_cabang_client'];
    $kode_cabang_client       = $row['kode_cabang_client'];
    $cabang_client            = $row['cabang_client'];
    $id_jabatan               = $row['id_jabatan'];
    $nama_jabatan             = $row['nama_jabatan'];
    $coverage_sim             = $row['coverage_sim'];
    $jenis_kelamin            = $row['jenis_kelamin'];
    $tanggal_kirim_candidate  = ($row['tanggal_kirim_candidate']);
    $tanggal_interview        = $row['tanggal_interview'];
    $tanggal_psikotest        = $row['tanggal_psikotest'];
    $hasil_seleksi_client     = $row['hasil_seleksi_client'];
    $remark                   = $row['remark'];
    $usia                     = $row['usia'];

    // $nama_candidate = str_replace(" ","'",$nama_candidate);
    // $nama_jabatan   = str_replace(" ","'",$nama_jabatan);
    // $cabang_client  = str_replace(" ","'", $cabang_client);
    // $nama_client    = str_replace(" ","'",$nama_client);
    
   $sql   = "INSERT INTO stg_sim_offering (id_so_induk,id_job,nomor_so_induk,jo_number,nama_candidate,id_client,kode_client,nama_client,id_cabang_client,
             kode_cabang_client,cabang_client,nama_jabatan,coverage_sim,usia,jenis_kelamin,tgl_kirim_candidate,tgl_interview,
             tgl_psikotes,hasil_seleksi_client,remark)
             VALUES('" . $id_so_induk . "','" . $id_jabatan . "','" . $nomor_so_induk . "','" . $jo_number . "','" . $nama_candidate . "','" . $id_client . "','" . $kode_client . "',
             '" . $nama_client . "','" . $id_cabang_client . "','" . $kode_cabang_client . "','" . $cabang_client . "','" .  $nama_jabatan . "',
             '" .  $coverage_sim . "','" .  $usia . "','" .   $jenis_kelamin . "','" .  $tanggal_kirim_candidate . "','" .  $tanggal_interview . "',
             '" .  $tanggal_psikotest . "','" .  $hasil_seleksi_client . "','" .  $remark . "')";
   mysqli_query($koneksi2,$sql);

    echo $sql ;
}
echo "stg_sim_offering : "  . $x;
?>